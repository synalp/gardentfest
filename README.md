## README

This project contains the sources (in md format) of the website of Claire Gardent's fest. These sources are compiled by [mkdocs](mkdocs.org) to produce a static website.

## Requirements

See requirements.txt

## Usage

To preview the website:

```bash
cd gardentfest
mkdocs serve 
```

To compile the website:
```bash
mkdocs build
```

The result will be store in the `site/` directory. 
Every time this directory is commited and pushed to the original repository on gitlab, it is copied to gitlabpages.inria.fr as well.
