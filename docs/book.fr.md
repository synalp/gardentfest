<br/>
# Livret
<br/>

### &Agrave; propos

Nous avons compilé un document retraçant les contributions de Claire au domaine de la linguistique informatique. Ce document contient des références aux articles dont Claire est co-autrice (groupés par affiliation et domaine), ainsi qu'une sélection de contributions de collègues de Claire.

Il est téléchargeable via le [lien suivant](img/gardentfest.pdf), et peut également être visualisé sur l'archive ouverte HAL [ici](https://hal.science/hal-04149469).

<br/>
### Crédits

Nous avons utilisé des informations disponibles en ligne sur le site de l'[ACL anthology](https://aclanthology.org/people/c/claire-gardent/).

Nous remercions particulièrement (par ordre alphabétique) Anna Liednikova, Bikash Gyawali, Eric Kow, Hélène Manuélian, Laura Perez-Beltrachini et Lina Rojas-Barahona pour leurs contributions.