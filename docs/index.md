<br/>
# Workshop in honour of Claire Gardent

## CNRS Silver Medal / ACL Fellow 2022

<br/>

!!! note ""
    Our colleague [Claire Gardent](https://members.loria.fr/CGardent/) has been awarded the [CNRS Silver Medal](https://www.centre-est.cnrs.fr/fr/personne/claire-gardent), and has been selected [ACL Fellow](https://www.aclweb.org/portal/content/acl-fellows-2022) 2022. To celebrate these awards, the SyNaLP team and the Loria laboratory are delighted to invite you to a one-day workshop in her honour. This event will take place on **Tuesday March, 21st**. <br/><br/>Participation is free of charge, but **[registration](registration.md) is mandatory** (and open until March, 15th).

<br/>

<center>
<div class="wise-iframe-wrapper">
<iframe width="560" height="315" src="https://www.youtube.com/embed/rVqrXupeuhw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
</center>

