<br/>
# Localisation
<br/>

## LORIA

![LORIA](img/CARROUSSEL-HOME.jpg)
<br/><br/>
La journée prendra place au LORIA, le laboratoire lorrain de rechreche en informatique et ses applications, qui est situé sur le campus de la faculté des sciences et technologies de l'Université de Lorraine. Son adresse est :

```
615 rue du Jardin Botanique
54602 Villers-lès-Nancy
tel. +33 (0)3 83 59 30 00
```
ses coordonnées GPS sont :

```
Latitude : 48.665507
Longitude : 6.156108
```

## Rejoindre le LORIA

<iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=6.155594587326051,48.66454348846887,6.159703731536866,48.666672797378745&amp;layer=mapnik&amp;marker=48.66560815416954,6.1576491594314575" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=48.66561&amp;mlon=6.15765#map=18/48.66561/6.15765&amp;layers=N">Afficher une carte plus grande</a></small>

À partir de la gare, le LORIA est accessible facilement par bus ou tramway.

*Accès en tramway*: Prendre la ligne [T1 &#x1F517;](https://www.reseau-stan.com/) à l'arrêt Gare en direction de Vandoeuvre CHU et sortir à l'arrêt Callot. Traverser alors le campus universitaire (voir carte ci-dessus). À partir de l'arrêt de tramway, aller à droite en direction du Lycée Jacques Callot, puis suivre la rue qui monte sur la gauche, pour enfin entrer sur le campus. Le LORIA est un des batiments en sommet de coline. Le tramway prend moins de 15 minutes depuis la gare et passe toutes les 10 minutes environ aux heureux de bureau.

*Accès par bus*: Prendre la ligne [Tempo 3 &#x1F517;](https://www.reseau-stan.com/) depuis Nancy centre (arrêt \"Poirel\") en direction de Villers Campus Science et descendre à l'arrêt Grande Corvée. Le LORIA est sur votre gauche, marcher 30 mètres (l'arrêt de bus se trouve sur la Rue du Jardin Botanique, à l'arrière du LORIA, l'entrée principale du LORIA est de l'autre côté). Le trajet prend moins de 15 minutes, les bus passent toutes les 10 minutes environ aux heureux de bureau.

## À voir

Il y a plusieurs endroits à visiter à Nancy et aux alentours. Le site de l'[Office du Tourisme](https://www.nancy-tourisme.fr/en/to-see-to-do/) contient une liste plus exhaustive des choses à voir. En voici un extrait :

* Nancy est reconnue pour ses oeuvres inscrites au patrimoine mondial de l'UNESCO : Place Stanislas, Place de la Carrière et Place d'Alliance.
* À proximité de la Place Stanislas se trouve la vieille ville.
* Nancy est également célèbre pour ses maisons Art Nouveau. 
* Pour une ballade, le parc de la Pépinière se trouve à proximité de la Place Stanislas.

## Voir aussi

Pour plus d'information, vous pouvez consulter le [site web du LORIA](https://www.loria.fr/en/loria/locate-us/).
