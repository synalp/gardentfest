<br/>
# Booklet
<br/>

### About

We compiled a document summarising Claire's contributions to the field of Computational Linguistics. This document contains references of articles co-authored by Claire (grouped by affiliation and topic), along with selected contributions from Claire's colleagues.

It can be downloaded via the following [link](img/gardentfest.pdf) or else be accessed in the HAL open archive [here](https://hal.science/hal-04149469).

<br/>
### Credits

We used information available on-line on the [ACL anthology](https://aclanthology.org/people/c/claire-gardent/).

We are particularly grateful to (by alphabetical order) Anna Liednikova, Bikash Gyawali, Eric Kow, Hélène Manuélian, Laura Perez-Beltrachini and Lina Rojas-Barahona for their contributions.
