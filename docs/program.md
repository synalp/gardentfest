<br/>
# Programme
<br/>

**Please note that this program may be subject to minor changes.**


!!! note ""
    **09:30** — Welcome coffee

    ---

    **10:00** — Workshop opening session chaired by [Yannick Toussaint](https://www.researchgate.net/profile/Yannick-Toussaint) (deputy head of Loria) and [Jean-Marie Pierrel](https://www.atilf.fr/recherche/portraits-entretiens/jean-marie-pierrel/) (former head of Loria and ATILF laboratories)

    ---

    **10:30** — Invited talk by [Bonnie Webber](https://homepages.inf.ed.ac.uk/bonnie/), University of Edinburgh, recipient of the [ACL](https://www.aclweb.org/portal/what-is-cl) Lifetime Achievement Award (2020) **[[slides]](img/slides_Bonnie.pdf)**

    &nbsp;&nbsp;&nbsp;&nbsp;Title: *Supporting Further Advances in Discourse-based Sentence Splitting.*

    &nbsp;&nbsp;&nbsp;&nbsp;Abstract: In recent work, Claire together with her student Liam Cripwell and colleague Joël Legrand explored sentence-splitting (mapping a complex sentence into a sequence of simpler sentences) from the dual perspectives of sentence-level syntax and discourse) [Cripwell et al, 2021; 2022].  I found the work particularly interesting, and have started speculating on whether the effort could be taken further by taking account of properties of version 3 of the Penn Discourse TreeBank (PDTB 3.0), which annotates several thousand more instances of intra-sentential discourse relations, many modified forms of discourse connectives, and cases where two discourse spans (sentences or clauses) have both an explicitly marked relation between them and one that has been left unmarked.<br/><br/>Liam Cripwell, Joël Legrand, and Claire Gardent (2021). Discourse-based sentence splitting. Findings of the Association for Computational Linguistics (EMNLP 2021), pages 261–273.<br/><br/>Liam Cripwell, Joël Legrand, and Claire Gardent (2022). Controllable Sentence Simplification via Operation Classification. Findings of the Association for Computational Linguistics (NAACL 2022), pages 2091–2103.

    ---

    **11:30** — Invited talk by [Marc Dymetman](https://scholar.google.com/citations?user=D6J5pooAAAAJ), Naverlabs, Grenoble **[[slides]](img/slides_Marc.pdf)**

    &nbsp;&nbsp;&nbsp;&nbsp;Title: *Controlling the Quality of Large Language Models: a Distributional Approach*

    &nbsp;&nbsp;&nbsp;&nbsp;Abstract: I will cover a line of work and collaborations, started a few years ago at NAVER Labs, where one augments a standard neural language model with constraints over the generative distribution. These help account for aspects of the training data that may be missed by these models (descriptive dimension) but also permit to introduce normative criteria (prescriptive dimension) controlling for biases, offensiveness, or other deficiencies of the standard training process.

    ---
    
    **12:30** — Lunch

    ---

    **14:00** — Invited talk by [Shashi Narayan](https://research.google/people/106924/), Google Brain, London **[[slides]](img/slides_Shashi.pdf)**

    &nbsp;&nbsp;&nbsp;&nbsp;Title: *Conditional Generation with Question-Answering Blueprint*

    &nbsp;&nbsp;&nbsp;&nbsp;Abstract: The ability to convey relevant and faithful information is critical for many tasks in conditional generation and yet remains elusive for neural seq-to-seq models whose outputs often reveal hallucinations and fail to correctly cover important details. In this work, we advocate planning as a useful intermediate representation for rendering conditional generation less opaque and more grounded.  We propose a new conceptualization of text plans as a sequence of question-answer (QA) pairs and enhance existing datasets (e.g., for summarization) with a QA *blueprint* operating as a proxy for content selection (i.e., what to say) and planning (i.e., in what order). We obtain blueprints automatically by exploiting state-of-the-art question generation technology and convert input-output pairs into input-blueprint-output tuples. We develop Transformer-based models, each varying in how they incorporate the blueprint in the generated output (e.g., as a global plan or iteratively). Evaluation across metrics and datasets demonstrates that blueprint models are more factual than alternatives which do not resort to planning and allow tighter control of the generation output.

    ---

    **15:00** — Invited talk by [Benoit Crabbé](https://www.linguist.univ-paris-diderot.fr/~bcrabbe/), Université Paris Cité, honorary member of the [Institut Universitaire de France](https://www.iufrance.fr/les-membres-de-liuf/membre/125-benoit-crabbe.html) (2014) **[[slides]](img/slides_Benoit.pdf)**

    &nbsp;&nbsp;&nbsp;&nbsp;Title: *The promise of language models for language sciences ? let's chat !*

    &nbsp;&nbsp;&nbsp;&nbsp;Abstract: The field of Computational linguistics is currently is going through a period of paradigm shift Large language models are now ubiquitous with chat GPT creating the last buzz. If you ask chat GPT its promises for the future of language sciences, you get the somewhat confident reply: "Large language models like myself hold great promise for the field of linguistics. They offer improved language understanding, access to vast amounts of data, automatic language analysis, and the ability to test linguistic theories. These tools can help linguists to gain new insights into how language works, identify patterns in language usage, and refine their linguistic theories." In this talk I will put in perspective some key modeling directions in computational linguistics: modeling language structure and modeling language in relation with the world knowledge. And I will explain how we eventually end up with the current language models. We will show that given what they are, current language models achieve sometimes surprising results with respect to the modeling of language structure and highlight some potential research perspectives in language sciences and some of their current limitations.

    ---

    **16:00** — Coffee break

    ---

    **16:30** — Invited talk by [Mark Steedman](https://homepages.inf.ed.ac.uk/steedman/), University of Edinburgh, recipient of the [ACL](https://www.aclweb.org/portal/what-is-cl) Lifetime Achievement Award (2018) **[[slides]](img/slides_Mark.pdf)**

    &nbsp;&nbsp;&nbsp;&nbsp;Title: *Inference in the Time of GPT &star;*

    &nbsp;&nbsp;&nbsp;&nbsp;Abstract: Large pretrained Language Models (LLM) such as GPT3 have upended NLP, calling into question many established methods.  In particular, they have been claimed to be capable of doing logical inference when fine-tuned on entailment datasets, or prompted with small numbers of examples of inferential tasks. <br/>The talk will review and assess these claims, and propose that we should not give up on alternative methods.<br/><br/>&star; With apologies to Gabriel Garcia Marquez.

    ---

    **17:30** — Workshop closing session chaired by [Christian Rétoré](https://www.lirmm.fr/~retore/) (Université de Montpellier / LIRMM)

    ---

    **18:00** — [Group picture](img/gardentfest.jpg) and cocktail

