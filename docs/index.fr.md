<br/>
# Journée en l'honneur de Claire Gardent

## Médaille d'argent du CNRS 2022 / ACL Fellow 2022

<br/>

!!! note ""
    À l'occasion de l'obtention par notre collègue [Claire Gardent](https://members.loria.fr/CGardent/), de la [médaille d'argent du CNRS](https://www.centre-est.cnrs.fr/fr/personne/claire-gardent) et du titre d'[ACL Fellow](https://www.aclweb.org/portal/content/acl-fellows-2022) 2022, l'équipe SyNaLP et le laboratoire Loria sont heureux de vous inviter à une journée scientifique en son honneur. Cette journée aura lieu le **mardi 21 mars**. <br/><br/>La participation est libre, mais l'**[inscription](registration.fr.md) obligatoire** (et ouverte jusqu'au 15 mars).

<br/>

<center>
<div class="wise-iframe-wrapper">
<iframe width="560" height="315" src="https://www.youtube.com/embed/rVqrXupeuhw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
</center>

