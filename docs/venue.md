<br/>
# Venue
<br/>

## LORIA

![LORIA](img/CARROUSSEL-HOME.jpg)
<br/><br/>
The workshop will take place at LORIA, the “Lorraine Research Laboratory in Computer Science and its Applications”, which is located on the campus of the Science Faculty of Nancy University. Its physical address is:

```
615 rue du Jardin Botanique
54602 Villers-lès-Nancy
tel. +33 (0)3 83 59 30 00
```
Its GPS coordinate are:

```
Latitude : 48.665507
Longitude : 6.156108
```

## Getting to LORIA

<iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=6.155594587326051,48.66454348846887,6.159703731536866,48.666672797378745&amp;layer=mapnik&amp;marker=48.66560815416954,6.1576491594314575" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=48.66561&amp;mlon=6.15765#map=18/48.66561/6.15765&amp;layers=N">Display a bigger map</a></small>

From the train station, LORIA is easily reached by bus or by tram.

*Access by tram*: Take the [T1 line &#x1F517;](https://www.reseau-stan.com/) at the Nancy Gare stop in the direction of Vandoeuvre CHU and get off at the stop Callot. Then traverse the University campus (see map below): from the tram stop, go right in the direction of Lycée Jacques Callot, then follow the street uphill (turning left), finally enter the campus. LORIA is the white building on the top of the hill. The tram takes less than 15 minutes and runs approximately every 7-15 minutes during working hours.

*Access by bus*: Take the  line [Tempo 3 &#x1F517;](https://www.reseau-stan.com/) from the Nancy centre (\"Poirel\" stop) in the direction of Villers Campus Science and get off at the stop Grande Corvée. LORIA is on your left, walk uphill about 30m. (The bus stop is in Rue du Jardin Botanique, facing the back side of LORIA, the main entrance to LORIA is on the other side.) The bus ride takes less than 15 minutes, buses run every 8-15 minutes during working hours.

Like many French cities, Nancy has a system of short-term bike rentals called Velostan. Rates are available here. The bike ride from the city center to LORIA takes about 20 minutes.

## Places to see

There are many places worth visiting in Nancy and around. We refer to the [Tourist Office](https://www.nancy-tourisme.fr/en/to-see-to-do/) for a more exhaustive presentation of those places, and give here our very short list.

* Nancy is famous around the world for its group of architectural masterpieces inscribed on the UNESCO World Heritage List: Place Stanislas, Place de la Carrière and Place d'Alliance.
* Not far away from Place Stanislas, you can find the old town.
* Nancy is also famous for its many Art Nouveau houses. 
* For a walk in a park, go to the Pépinière Park, a large green area a few meters away from Place Stanislas and the old town.

## See also

For more information, you can also look at [LORIA's website](https://www.loria.fr/en/loria/locate-us/).
